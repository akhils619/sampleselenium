import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestInputBoxes {

	WebDriver driver;

	@Before
	public void setUp() throws Exception {
		// 1. Configure Selenium to talk to Chrome
		System.setProperty("webdriver.chrome.driver", "/Users/akhilsraj/Desktop/chromedriver");
		driver = new ChromeDriver();

		// 2. Enter the website you want to go to
		String baseUrl = "https://www.seleniumeasy.com/test/basic-first-form-demo.html";

		// 3. Open Chrome and go to the base url;
		driver.get(baseUrl);
	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(10000);  //pause for 1 second before closing the browser
		driver.close();
	}

	@Test
	public void testSingleInputField() {
		// 1. Configure Selenium to talk to Chrome
		System.setProperty("webdriver.chrome.driver", "/Users/akhilsraj/Desktop/chromedriver");
		WebDriver driver = new ChromeDriver();

		// 2. Enter the website you want to go to
		String baseUrl = "https://www.seleniumeasy.com/test/basic-first-form-demo.html";

		// 3. Open Chrome and go to the base url;
		driver.get(baseUrl);
		WebElement inputBox = driver.findElement(By.id("user-message"));
		// 4b. Put the user name in there
		inputBox.sendKeys("This is all nonsense");

		// 5. Enter a password
		// ---------------------
		// 5a. Find the password box
		WebElement showMessageButton = driver.findElement(By.cssSelector("form#get-input button"));
		// 5b. Put the password in there
		showMessageButton.click();

		WebElement displayMessage = driver.findElement(By.id("display"));
		String message = displayMessage.getText();

		assertEquals("This is all nonsense", message);

	}

	@Test
	public void testTwoInputField() {
		// 1. Configure Selenium to talk to Chrome
		System.setProperty("webdriver.chrome.driver", "/Users/akhilsraj/Desktop/chromedriver");
		WebDriver driver = new ChromeDriver();

		// 2. Enter the website you want to go to
		String baseUrl = "https://www.seleniumeasy.com/test/basic-first-form-demo.html";

		// 3. Open Chrome and go to the base url;
		driver.get(baseUrl);
		WebElement firstValue = driver.findElement(By.id("sum1"));
		// 4b. Put the user name in there
		firstValue.sendKeys("2");
		;
		;

		WebElement secondValue = driver.findElement(By.id("sum2"));
		// 4b. Put second user name in there
		secondValue.sendKeys("4");

		WebElement addButton = driver.findElement(By.cssSelector("form#gettotal button"));
		// 5b. Put the password in there
		addButton.click();

		WebElement displayMessage = driver.findElement(By.id("displayvalue"));
		String message = displayMessage.getText();

		assertEquals("6", message);

	}
}
